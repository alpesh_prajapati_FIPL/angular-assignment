
# Angular Assignment

This is test project to demonstrate the 30 images at one time with pagination via API in Angular 17. It contains all the required codes.

## Get Started.
To get started needs to get the repo from Github and move into directory to run the project in IDE.

### Clone the repo.
```
- git clone https://gitlab.com/alpesh_prajapati_FIPL/angular-assignment.git

- cd assignment
```

### Install npm packages
install the npm packages and ng serve command will help you to run the project as in localhost. 
```
- npm install

- npm serve
```

The npm server commands will build the application and will be run on 4200 port, If the port is already in use it may give you a random port number to demonstrate the application into browser. 


To close or shut the terminal ```CTRL + C``` will be the command.


### Created service file to fetch the API

Each service will required to fetch the API to get the latest data.

```Created
export class ListimagesService {

  private apiURL = "https://picsum.photos/v2/list";

  constructor(private _listImage: HttpClient) { }

  getImageList(){
    return this._listImage.get(this.apiURL)
  };
}

```


### Display Data

Once the Fetch API done data needs to display in template/HTML file. 

```Created
<div class="item" *ngFor="let item of item">
    
    <div class="thumbnail" [ngStyle]="{'background-image': 'url(' + item.download_url +')'}">
        <h3>{{ item.author }}</h3>
    </div>

</div>
```
## Documentation

For any query please email on alpesh.prajapati@flydocs.aero or drop a message in Teams :) 

