import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { response } from 'express';
import { ModalpopupComponent } from '../../modalpopup/modalpopup.component';
import { ListimagesService } from '../../services/listimages.service';

@Component({
  selector: 'app-listitem',
  standalone: true,
  imports: [CommonModule, ModalpopupComponent],
  templateUrl: './listitem.component.html',
  styleUrl: './listitem.component.scss'
})
export class ListitemComponent {

  currentpage = 1;
  totalpages = 0;
  limit = 30;

  item: any;
  isModalOpen: boolean = false;
  haveGotDetails: boolean = false;
  imageDetailsObj: any;

  imgPopUpDetail: any;

  constructor(private _imgList: ListimagesService) { }

  ngOnInit() {
    this.fetchImageList();
  }
  /* GET IMAGE LIST FROM API */
  fetchImageList() {
    this._imgList.getImageList().subscribe(response => {
      this.item = response;
    })
  }

  /* GET PAGE NUMBER FROM API */
  fetchData(page: number) {
    this._imgList.getPage(page).subscribe(response => {
      this.item = response;
    })
  }

  /* GET DETAILS OF THE IMAGE FROM ID VIA API */
  fetchImageDetails(id: number) {
    this._imgList.getImageDetail(id).subscribe(response => {
      this.imgPopUpDetail = response;
      this.haveGotDetails = true;
    })
  }

  /* PAGINATE THE LIST */
  paginate(str: string) {
    if (str === "prev" && this.currentpage >= 1) {
      this.currentpage--;
      this.fetchData(this.currentpage);
      console.log(this.currentpage)
    } else if (str === "next") {
      this.currentpage++;
      this.fetchData(this.currentpage);
      console.log(this.currentpage)
    }
  }

  /* OPEN MODAL POPUP TO GET THE IMAGE DETAILS */
  openModal(id: number) {
    this.isModalOpen = true;
    this.haveGotDetails = false;
    this.fetchImageDetails(id)
  }

  /* CLOSE THE LIST */
  closeModal() {
    this.isModalOpen = false;
  }

}
