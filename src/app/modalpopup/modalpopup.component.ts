import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-modalpopup',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './modalpopup.component.html',
  styleUrl: './modalpopup.component.scss'
})
export class ModalpopupComponent {

  @Input() imageObj: any;
  @Input() isLoaded : boolean = false
  @Output() closeModal = new EventEmitter<void>();
 
  close() {
    this.closeModal.emit();
  }
  
}
