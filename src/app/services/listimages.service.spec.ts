import { TestBed } from '@angular/core/testing';

import { ListimagesService } from './listimages.service';

describe('ListimagesService', () => {
  let service: ListimagesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ListimagesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
