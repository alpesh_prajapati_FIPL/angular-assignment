import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ListimagesService {

  private apiURL = "https://picsum.photos/v2/list";
  private globalURL = "https://picsum.photos";

  constructor(private _listImage: HttpClient) { }

  /* IMAGE LIST API */
  getImageList() {
    return this._listImage.get(this.apiURL)
  };

  /* GET THE PAGE API */
  getPage(page: number) {
    return this._listImage.get(`${this.apiURL}?page=${page}`);
  };

  /* IMAGE DETAILS API */
  getImageDetail(id: number) {
    return this._listImage.get(`${this.globalURL}/id/${id}/info`);
  };

}
